import requests
import json

def azure_face_recognition(id_image_path, selfie_image_path, subscription_key, endpoint):
    detect_url = endpoint + '/face/v1.0/detect'
    verify_url = endpoint + '/face/v1.0/verify'

  
    headers = {
        'Ocp-Apim-Subscription-Key': subscription_key,
        'Content-Type': 'application/octet-stream'
    }

  
    with open(id_image_path, 'rb') as f:
        id_image_data = f.read()


    with open(selfie_image_path, 'rb') as f:
        selfie_image_data = f.read()


    id_face = detect_face(id_image_data, detect_url, headers)
    selfie_face = detect_face(selfie_image_data, detect_url, headers)
    verify_result = verify_faces(id_face[0]['faceId'], selfie_face[0]['faceId'], verify_url, headers)
    return verify_result

def detect_face(image_data, url, headers):
    body = image_data

 
    params = {
        'returnFaceId': 'true',
        'returnFaceLandmarks': 'false',
        'returnFaceAttributes': ''
    }
    response = requests.post(url, params=params, headers=headers, data=body)
    faces = json.loads(response.text)
    return faces

def verify_faces(face_id1, face_id2, url, headers):
    body = {
        'faceId1': face_id1,
        'faceId2': face_id2
    }

    response = requests.post(url, headers=headers, json=body)
    verify_result = json.loads(response.text)
    return verify_result


id_image_path = "C:\Users\Ajith\Documents\FaceRecognition/Aadhar.pdf"
selfie_image_path = 'C:\Users\Ajith\Documents\FaceRecognition/Image.jpg.jpeg'
subscription_key = 'YOUR_SUBSCRIPTION_KEY'
endpoint = 'https://YOUR_API_ENDPOINT'

result = azure_face_recognition(id_image_path, selfie_image_path, subscription_key, endpoint)

print(result)
